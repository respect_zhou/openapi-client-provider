package com.acooly.openapi.client.provider.wsbank.bkmerchanttradeRefund;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradeRefundRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkmerchanttradeRefundResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradeRefundRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradeRefundRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "wsbankBkmerchanttradePayCloseTest")
public class WsbankBkmerchanttradeRefundTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 测试退款接口
     */
    @Test
    public void bkmerchanttradeRefund() {
    	WsbankBkmerchanttradeRefundRequestBody requestBody = new WsbankBkmerchanttradeRefundRequestBody();
    	requestBody.setOutTradeNo("o18060614361588840003");
    	requestBody.setMerchantId("226801000000103460124");
    	requestBody.setOutRefundNo(Ids.oid());
    	requestBody.setRefundAmount("1");
    	requestBody.setDeviceCreateIp("172.16.1.88");
        WsbankBkmerchanttradeRefundRequestInfo requestInfo = new WsbankBkmerchanttradeRefundRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkmerchanttradeRefundRequest request = new WsbankBkmerchanttradeRefundRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkmerchanttradeRefundResponse response = wsbankApiService.bkmerchanttradeRefund(request);
        System.out.println("测试退款接口："+ JSON.toJSONString(response));
    }
}
