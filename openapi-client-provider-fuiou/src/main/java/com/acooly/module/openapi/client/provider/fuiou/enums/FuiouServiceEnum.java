/**
 * create by zhangpu
 * date:2015年3月24日
 */
package com.acooly.module.openapi.client.provider.fuiou.enums;

/**
 * @author zhangpu
 *
 */
public enum FuiouServiceEnum {

	AppQuickDeposit("app/500002", "APP快捷充值"),
	
	AppQuickWithdraw("app/500003", "APP提现"),
	
	Deposit("500002", "网银充值"),
	
	Withdraw("500003", "提现"),

	Balance("BalanceAction", "余额查询"),
	
	Freeze("freeze", "冻结"),
	
	TransferBuAndFreeze("transferBuAndFreeze","划拨冻结"),
	
	UnFreeze("unFreeze", "解冻"),
	
	TransferBu("transferBu", "划拨"),
	
	TransferBmu("transferBmu","转账"),
	
	TransferBuAndFreeze2Freeze("transferBuAndFreeze2Freeze","冻结转冻结");
	

	private String key;
	private String val;

	private FuiouServiceEnum(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
