/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.provider.fuiou.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.fuiou.FuiouApiServiceClient;
import com.acooly.module.openapi.client.provider.fuiou.OpenAPIClientFuiouProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 富友 支付网关异步通知分发器
 *
 * @author liuyuxiang
 * @date 2016年5月12日
 */
@Service
public class FuiouNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private OpenAPIClientFuiouProperties openAPIClientFuiouProperties;

    @Resource(name = "fuiouApiServiceClient")
    private FuiouApiServiceClient apiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return Strings.substringAfterLast(notifyUrl, openAPIClientFuiouProperties.getNotifyUrlPrefix());
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return apiServiceClient;
    }
}
