package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 20:22
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.TRADE_QUERY,type = ApiMessageType.Response)
public class YipayTradeQueryResponse extends YipayResponse {

    /**
     * 交易状态编码
     * 交易状态编码
     * 000:成功
     * 001:初始化
     * 002:处理中
     * 004:失败
     */
    @NotBlank
    @Size(max = 32)
    @YipayAlias(value = "statCode")
    private String statCode;

    /**
     * 交易结果编码
     */
    @NotBlank
    @Size(max = 6)
    @YipayAlias(value = "resultCode")
    private String resultCode;

    /**
     * 交易结果描述
     * 失败的提示信息比较多种，无法确定最大长度
     */
    @YipayAlias(value = "resultMsg")
    private String resultMsg;

    /**
     * 原商户订单号
     * 原交易或退款的商户订单号
     */
    @YipayAlias(value = "originReqSeq")
    private String originReqSeq;

    /**
     * 翼支付订单号（对账单号）
     * 翼支付系统生成的订单号，用于对账
     */
    @YipayAlias(value = "coreOrderCode")
    private String coreOrderCode;

    /**
     * 请求发起时间
     * 翼支付系统生成的订单号，用于对账
     */
    @NotBlank
    @YipayAlias(value = "reqTime")
    private String reqTime;

    /**
     * 交易完成时间
     */
    @YipayAlias(value = "tradeFinTime")
    private String tradeFinTime;

    /**
     * 交易金额（分）
     * 交易金额(不含手续费)，只能为小于18位的非零正整数，以分为单位，只支持人民币
     */
    @NotBlank
    @YipayAlias(value = "tradeAmount")
    private String tradeAmount;
}
