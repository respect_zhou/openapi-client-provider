/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.shengpay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum ShengpayCardTypeEnum implements Messageable {

    CARD_TYPE_IC("IC","身份证"),
    CARD_TYPE_TIC("TIC","临时身份证"),
    CARD_TYPE_PP("PP","护照"),
    CARD_TYPE_SC("SC","士兵证"),
    CARD_TYPE_AOC("AOC","军官证"),
    CARD_TYPE_ACC("ACC","军人文职干部证"),
    CARD_TYPE_POC("POC","警官证"),
    CARD_TYPE_APC("APC","武警证"),
    CARD_TYPE_HMP("HMP","港澳居民来往内地通行证"),
    CARD_TYPE_RB("RB","户口簿"),
    CARD_TYPE_TWP("TWP","台湾居民来往大陆通行证/台胞证"),
    CARD_TYPE_TWR("TWR","台湾回乡证"),
    CARD_TYPE_FPP("FPP","外国护照"),
    CARD_TYPE_FR("FR","外国人永久居留证"),
    ;

    private final String code;
    private final String message;

    private ShengpayCardTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (ShengpayCardTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static ShengpayCardTypeEnum find(String code) {
        for (ShengpayCardTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("FudianAuditStatusEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<ShengpayCardTypeEnum> getAll() {
        List<ShengpayCardTypeEnum> list = new ArrayList<ShengpayCardTypeEnum>();
        for (ShengpayCardTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (ShengpayCardTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }


}
