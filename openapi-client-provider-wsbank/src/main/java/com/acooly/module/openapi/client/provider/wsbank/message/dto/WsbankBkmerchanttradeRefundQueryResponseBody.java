package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradeRefundQueryResponseBody implements Serializable {

	private static final long serialVersionUID = -88986219669638671L;

	/**
	 * 返回码组件。当ResultStatus=S时才有后续的参数返回。
	 */
	@XStreamAlias("RespInfo")
	private WsbankResponseInfo responseInfo;

	/**
	 * 外部交易号
	 */
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;

	/**
	 * 退款外部交易号。由合作方生成，同笔退款交易，交易状态未明需要重试时，使用同一个交易号。
	 */
	@XStreamAlias("OutRefundNo")
	private String outRefundNo;

	/**
	 * 退款订单号。该字段仅退款成功时返回
	 */
	@XStreamAlias("RefundOrderNo")
	private String refundOrderNo;

	/**
	 * 退款交易状态
	 */
	@XStreamAlias("TradeStatus")
	private String tradeStatus;

	/**
	 * 合作方机构号（网商银行分配）
	 */
	@XStreamAlias("IsvOrgId")
	private String isvOrgId;

	/**
	 * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
	 */
	@XStreamAlias("MerchantId")
	private String merchantId;

	/**
	 * 支付返回的订单号
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;

	/**
	 * 退款金额
	 */
	@XStreamAlias("RefundAmount")
	private String refundAmount;

	/**
	 * 退款原因
	 */
	@XStreamAlias("RefundReason")
	private String refundReason;

	/**
	 * 操作员ID
	 */
	@XStreamAlias("OperatorId")
	private String operatorId;

	/**
	 * 终端设备号(门店号或收银设备ID)。
	 */
	@XStreamAlias("DeviceId")
	private String deviceId;

	/**
	 * 退款完成时间
	 */
	@XStreamAlias("GmtRefundment")
	private String gmtRefundment;

}
