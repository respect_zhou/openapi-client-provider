/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.wsbank.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import org.springframework.stereotype.Service;

/**
 * @author zhike
 */
@Service
public class WsbankRequestMarshall extends WsbankMarshallSupport implements ApiMarshal<String, WsbankRequest> {

    @Override
    public String marshal(WsbankRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
