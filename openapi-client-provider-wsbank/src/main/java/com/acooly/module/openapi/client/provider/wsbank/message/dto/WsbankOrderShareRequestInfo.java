package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("request")
public class WsbankOrderShareRequestInfo implements Serializable {

	private static final long serialVersionUID = 5694513484515956023L;

	/**
     * 请求报文头
     */
    @NotNull
    @XStreamAlias("head")
    private WsbankHeadRequest headRequest;

    @NotNull
    @XStreamAlias("body")
    private WsbankOrderShareRequestBody wsbankOrderShareRequestBody;
}
