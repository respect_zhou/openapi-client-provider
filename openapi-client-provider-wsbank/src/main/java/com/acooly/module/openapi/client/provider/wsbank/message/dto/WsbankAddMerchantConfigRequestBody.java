package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 16:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankAddMerchantConfigRequestBody implements Serializable {


//    OutTradeNo	外部交易号	String	64	ME
//    IsvOrgId	合作方机构号（网商银行分配）。	String	32	M
//    MerchantId	商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。	String	32	M
//    Appid	关联APPID	String	32	组合
//    Path	JS API支付授权目录 ，要求符合URI格式规范，每次添加一个支付目录，最多5个	String	256	组合
//    SubscribeAppid	特约商户或渠道号的公众号APPID	String	32	组合


    /**
     *合作方机构号（网商银行分配）
     */
    @Size(max = 64)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;


    @Size(max = 64)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    @Size(max = 64)
    @XStreamAlias("Appid")
    private String appid;

    @Size(max = 256)
    @XStreamAlias("Path")
    private String path;

    /**
     * 特约商户或渠道号的公众号APPID	String	32	组合
     */
    @Size(max = 32)
    @XStreamAlias("SubscribeAppid")
    private String subscribeAppid;





}


