/**
 * create by zhike
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.baofup;

import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPNotify;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceVersionEnum;
import com.acooly.module.openapi.client.provider.baofup.message.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhike
 */
@Service
public class BaoFuPApiService {

    @Resource(name = "baofuPApiServiceClient")
    private BaoFuPApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientBaoFuPProperties openAPIClientBaoFuPProperties;

    /**
     * 协议支付预绑卡
     * @param request
     * @return
     */
    public BaoFuProtocolPayApplyBindCardResponse protocolPayApplyBindCard(BaoFuProtocolPayApplyBindCardRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_PAY_APPLY_BIND_CARD.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_PAY_APPLY_BIND_CARD.getKey());
        request.setAccInfo(request.getAccInfo());
        return (BaoFuProtocolPayApplyBindCardResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议支付确认绑卡
     * @param request
     * @return
     */
    public BaoFuProtocolPayConfirmBindCardResponse protocolPayConfirmBindCard(BaoFuProtocolPayConfirmBindCardRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_PAY_CONFIRM_BIND_CARD.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_PAY_CONFIRM_BIND_CARD.getKey());
        request.setApplyCode(request.getApplyCode());
        return (BaoFuProtocolPayConfirmBindCardResponse)apiServiceClient.execute(request);
    }

    /**
     * 直接协议支付
     * @param request
     * @return
     */
    public BaoFuProtocolPayResponse protocolPay(BaoFuProtocolPayRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_PAY.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_PAY.getKey());
        request.setNotifyUrl(openAPIClientBaoFuPProperties.getDomainUrl()+BaoFuPConstants.NOTIFY_SUB_URL + BaoFuPServiceEnum.PROTOCOL_PAY.getCode());
        return (BaoFuProtocolPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 协议支付预支付类交易
     * @param request
     * @return
     */
    public BaoFuProtocolApplyPayResponse protocolApplyPay(BaoFuProtocolApplyPayRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_APPLY_PAY.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_APPLY_PAY.getKey());
        request.setNotifyUrl(openAPIClientBaoFuPProperties.getDomainUrl()+BaoFuPConstants.NOTIFY_SUB_URL + BaoFuPServiceEnum.PROTOCOL_PAY.getCode());
        return (BaoFuProtocolApplyPayResponse)apiServiceClient.execute(request);
    }


    /**
     * 协议支付确认支付类交易
     * @param request
     * @return
     */
    public BaoFuProtocolConfirmPayResponse protocolConfirmPay(BaoFuProtocolConfirmPayRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_CONFIRM_PAY.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_CONFIRM_PAY.getKey());
        request.setApplyCode(request.getApplyCode());
        return (BaoFuProtocolConfirmPayResponse)apiServiceClient.execute(request);
    }

    /**
     * 解除银行卡绑定
     * @param request
     * @return
     */
    public BaoFuProtocolPayUnBindCardResponse protocolPayUnBindCard(BaoFuProtocolPayUnBindCardRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_PAY_UNBIND_CARD.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_PAY_UNBIND_CARD.getKey());
        return (BaoFuProtocolPayUnBindCardResponse)apiServiceClient.execute(request);
    }

    /**
     * 查询绑定关系类交易
     * @param request
     * @return
     */
    public BaoFuProtocolPayQueryBindCardResponse protocolPayQueryBindCard(BaoFuProtocolPayQueryBindCardRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_PAY_QUERY_BIND_CARD.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_PAY_QUERY_BIND_CARD.getKey());
        return (BaoFuProtocolPayQueryBindCardResponse)apiServiceClient.execute(request);
    }

    /**
     * 支付结果查询
     * @param request
     * @return
     */
    public BaoFuProtocolPayQueryResponse protocolPayQuery(BaoFuProtocolPayQueryRequest request) {
        request.setService(BaoFuPServiceEnum.PROTOCOL_PAY_QUERY.getCode());
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuPServiceEnum.PROTOCOL_PAY_QUERY.getKey());
        return (BaoFuProtocolPayQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 对账文件下载
     *
     * @param request
     * @return
     */
    public BaoFuPBillDowloadResponse billDownload(BaoFuPBillDowloadRequest request) {
        request.setVersion(BaoFuPServiceVersionEnum.VERSION_4002.getCode());
        return apiServiceClient.billDownload(request);
    }

    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public BaoFuPNotify notice(HttpServletRequest request, String serviceKey) {
        return apiServiceClient.notice(request, serviceKey);
    }
}
