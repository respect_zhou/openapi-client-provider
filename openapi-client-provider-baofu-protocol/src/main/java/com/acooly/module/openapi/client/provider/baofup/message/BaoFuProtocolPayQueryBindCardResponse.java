package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/13 14:29
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_QUERY_BIND_CARD,type = ApiMessageType.Response)
public class BaoFuProtocolPayQueryBindCardResponse extends BaoFuPResponse {

    /**
     * 协议列表
     * 格式：签约协议号|用户ID|银行卡号|银行编码|银行名称; 签约协议号|用户ID|银行卡号|银行编码|银行名称
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @BaoFuPAlias(value = "protocols",isDecode = true)
    private String protocolNo;
}
