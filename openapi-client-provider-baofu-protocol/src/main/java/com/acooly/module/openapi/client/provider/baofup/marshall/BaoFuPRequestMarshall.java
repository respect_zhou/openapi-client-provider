/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.baofup.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.utils.StringHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.SortedMap;

/**
 * @author zhike
 */
@Service
@Slf4j
public class BaoFuPRequestMarshall extends BaoFuPMarshallSupport implements ApiMarshal<String, BaoFuPRequest> {

    @Override
    public String marshal(BaoFuPRequest source) {
        beforeMarshall(source);
        SortedMap<String, String> requestDataMap = doMarshall(source);
        String requestStr = StringHelper.getRequestStr(requestDataMap);
        return requestStr;
    }
}
