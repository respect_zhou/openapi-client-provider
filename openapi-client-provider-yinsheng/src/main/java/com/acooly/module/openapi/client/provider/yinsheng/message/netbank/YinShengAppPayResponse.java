package com.acooly.module.openapi.client.provider.yinsheng.message.netbank;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/9 10:09
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.APP_PAY, type = ApiMessageType.Response)
public class YinShengAppPayResponse extends YinShengResponse{

    /**
     *订单号
     * 银盛支付合作商户网站唯一订单号。
     */
    private String out_trade_no;

    /**
     *银盛流水号
     * 银盛支付平台的交易流水
     */
    private String trade_no;

    /**
     *状态码
     * 交易目前所处的状态。成功状态的值： TRADE_SUCCESS|TRADE_CLOSED等具体详情看错误码中的交易状态详解
     */
    private String trade_status;

    /**
     *订单总金额
     * 该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
     */
    private String total_amount;

    /**
     * 币种
     */
    private String currency;

    /**
     * 支付参数信息
     * 	Json格式字符串，SDK调起微信或支付宝支付所需要的支付参数信息。
     {"sign":"签名",
     "timestamp":"时间戳",
     "noncestr":"随机字符串",
     "partnerid":"微信支付分配的商户号",
     "prepayid":"微信返回的支付交易会话ID",
     "package":"Sign=WXPay",
     "appid":"微信公众号帐号"}
     */
    private String pay_info;
}
