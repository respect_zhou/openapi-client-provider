package com.acooly.module.openapi.client.provider.yinsheng.message.dto;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class YinShengBillDownLoadInfo implements Serializable {

    /**
     * 入账的时间 yyyy-MM-dd
     */
    private String account_date;
}
