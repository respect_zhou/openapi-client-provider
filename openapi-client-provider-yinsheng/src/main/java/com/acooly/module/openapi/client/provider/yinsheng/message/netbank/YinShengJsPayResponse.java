package com.acooly.module.openapi.client.provider.yinsheng.message.netbank;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/8 19:17
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.WECHAT_JSPAY, type = ApiMessageType.Response)
public class YinShengJsPayResponse extends YinShengResponse{

    /**
     *订单号
     * 银盛支付合作商户网站唯一订单号。
     */
    private String out_trade_no;

    /**
     *银盛流水号
     * 银盛支付平台的交易流水
     */
    private String trade_no;

    /**
     *状态码
     * 交易目前所处的状态。成功状态的值： TRADE_SUCCESS|TRADE_CLOSED等具体详情看错误码中的交易状态详解
     */
    private String trade_status;

    /**
     * Json字符串
     * Json格式字符串，作用于原生态的js支付时的参数
     * 预支付单
     */
    private String jsapi_pay_info;
}
