package com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:30.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("QUERY_DETAIL")
public class QdReqQueryDetail {
    /**
     *交易明细号
     */
    @XStreamAlias("QUERY_DETAIL_SN")
    private String queryDetailSn;

}
