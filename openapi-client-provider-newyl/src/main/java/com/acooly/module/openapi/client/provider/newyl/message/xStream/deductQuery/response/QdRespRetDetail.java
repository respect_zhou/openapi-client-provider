package com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/1/26 15:30.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("QUERY_DETAIL")
public class QdRespRetDetail {
    /**
     *记录序号
     */
    @XStreamAlias("SN")
    private String sn;
    /**
     *账号
     */
    @XStreamAlias("ACCOUNT")
    private String account;
    /**
     *账号名
     */
    @XStreamAlias("ACCOUNT_NAME")
    private String accountName;
    /**
     *金额
     */
    @XStreamAlias("AMOUNT")
    private String amount;
    /**
     *自定义用户号
     */
    @XStreamAlias("CUST_USERID")
    private String custUserId;
    /**
     *备注
     */
    @XStreamAlias("REMARK")
    private String remark;
    /**
     *交易时间
     */
    @XStreamAlias("COMPLETE_TIME")
    private String completeTime;
    /**
     *清算日期
     */
    @XStreamAlias("SETT_DATE")
    private String settDate;
    /**
     *返回码
     */
    @XStreamAlias("RET_CODE")
    private String retCode;
    /**
     *错误文本
     */
    @XStreamAlias("ERR_MSG")
    private String errMsg;
    /**
     *DATE_SETTLMT
     */
    @XStreamAlias("DATE_SETTLMT")
    private String dateSetTlmt;
    /**
     *备注域1
     */
    @XStreamAlias("RESERVE1")
    private String reserve1;
    /**
     *备注域2
     */
    @XStreamAlias("RESERVE2")
    private String reserve2;

}
