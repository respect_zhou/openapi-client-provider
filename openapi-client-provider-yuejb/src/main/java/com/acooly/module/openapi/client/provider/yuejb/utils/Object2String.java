package com.acooly.module.openapi.client.provider.yuejb.utils;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
public class Object2String {
    public static String convertString(Object object) {
        if (object == null) {
            return null;
        }
        return object.toString();
    }
}
