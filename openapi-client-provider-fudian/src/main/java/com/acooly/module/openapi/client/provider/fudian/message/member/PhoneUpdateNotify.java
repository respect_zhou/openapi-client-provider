/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 03:05:51 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-10 03:05:51
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.PHONE_UPDATE ,type = ApiMessageType.Notify)
public class PhoneUpdateNotify extends FudianNotify {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 新手机号
     * 需要修改的新的手机号码
     */
    @NotEmpty
    @Length(min = 11,max=11)
    private String newPhone;

    /**
     * 修改方式
     * 修改手机方式：
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String type;

    /**
     * 状态
     * 1代表更新手机号成功
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;
}