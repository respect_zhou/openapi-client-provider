/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOAN_FULL ,type = ApiMessageType.Request)
public class LoanFullRequest extends FudianRequest {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 借款管理费
     * 标的借款管理费，单位:元,保留2位有效数字
     */
    @NotEmpty
    @Length(max=20)
    private String loanFee;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max=32)
    private String loanTxNo;

    /**
     * 发标的订单日期
     * 发标的时候的订单流水日期
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String loanOrderDate;

    /**
     * 发标的订单流水号
     * 发标的时候的订单流水号
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String loanOrderNo;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;

    /**
     * 预计还款时间
     * 标的预计还款时间，请按照标的预计还款时间填写，商户要保证此项数据的真实性。格式：yyyyMMdd
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String expectRepayTime;
}