/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 09:22 创建
 */
package com.acooly.module.openapi.client.provider.fudian.domain;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.alibaba.fastjson.annotation.JSONField;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Transient;
import java.util.Date;

/**
 * 富滇银行存管基础报文
 *
 * @author zhangpu 2017-09-22 09:22
 */
public class FudianMessage implements ApiMessage {

    /**
     * 采用RESTLET的URL路径作为服务唯一标志（服务名）
     */
    @Transient
    @JSONField(serialize = false)
    @NotEmpty
    @Length(max = 128)
    private String service;

    /**
     * 接入商户标志（商户号:merchantNo）
     */
    @NotEmpty
    @Transient
    @JSONField(serialize = false)
    private String partner;

    /**
     * 订单日期
     * 单笔交易的日期，辅助标识
     */
    @NotEmpty
    @Length(min = 8, max = 8)
    private String orderDate = Dates.format(new Date(), "yyyyMMdd");

    /**
     * 订单流水号
     * 单笔交易的唯一标识
     */
    @NotEmpty
    @Length(min = 20, max = 20)
    private String orderNo = Ids.getDid(20);

    /**
     * 参数扩展域
     * 该字段在交易完成后由本平台原样返回。注意：如果该字段中包含了中文字符请对该字段的数据进行Base64编码后再使用
     */
    @Length(max=256)
    private String extMark;

    public void doCheck() {

    }

    @Override
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }


    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
