package com.acooly.module.openapi.client.provider.fuyou.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/3/21 13:39
 */
@Getter
@Setter
@XStreamAlias("plain")
public class FuyouNetBankSynchroQueryInfo implements Serializable{

    /**
     * 商户订单号
     * 客户支付后商户网站产生的一个唯
     * 一的定单号，该订单号应该在相当
     * 长的时间内不重复。富友通过订单
     * 号来唯一确认一笔订单的重复性.
     */
    @XStreamAlias("order_id")
    @NotBlank
    @Size(max = 30)
    private String orderId;

    /**
     * 错误代码
     * 0000 表示成功 其他失败
     */
    @XStreamAlias("order_pay_code")
    @NotBlank
    @Size(max = 4)
    private String orderPayCode;

    /**
     * 错误中文描述
     */
    @XStreamAlias("order_pay_error")
    @NotBlank
    @Size(max = 40)
    private String orderPayError;

    /**
     * 订单状态
     * ‘00’ – 订单已生成(初始状态)
     * ‘01’ – 订单已撤消
     * ‘02’ – 订单已合并
     * ‘03’ – 订单已过期
     * ‘04’ – 订单已确认(等待支付)
     * ‘05’ – 订单支付失败
     * ‘11’ – 订单已支付
     */
    @XStreamAlias("order_st")
    @NotBlank
    @Size(max = 2)
    private String orderSt;

    /**
     * 保留字段
     */
    @XStreamAlias("resv1")
    @NotBlank
    @Size(max = 40)
    private String resv1;

    /**
     * 富友流水号
     * 供商户查询支付交易状态及对账用
     */
    @XStreamAlias("fy_ssn")
    @NotBlank
    @Size(max = 12)
    private String fySsn;

}
