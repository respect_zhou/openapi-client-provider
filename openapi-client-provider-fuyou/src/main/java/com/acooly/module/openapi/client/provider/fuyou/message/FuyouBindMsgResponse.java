package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_BIND_MSG,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouBindMsgResponse extends FuyouResponse{

    /**
     * 商户流水号，保持唯一
     */
    @XStreamAlias("MCHNTSSN")
    @NotBlank
    @Size(max = 30)
    private String orderNo;

    /**
     * 签名类型
     */
    @XStreamAlias("SIGNTP")
    private String signType;
}
