<!-- title: 威富通支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-wft</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

* 直接采用wft的SDK，然后扩展异步通知处理
#使用方式：
###在自己工程注入FuyouApiService

###此sdk提供威富通的微信正扫、微信反扫、微信公众号、微信APP、支付宝正扫、支付宝反扫、支付宝APP支付、订单关闭、订单撤销、订单查询已经对账文件下载

###密钥加载
* 威富通的验签签名统一使用MD5的方式
   
###示例
* 本sdk中微信公众号、支付宝主扫支付、微信主扫、微信APP支付的异步通知地址固定值，所以调用的时候不需要传入notify_url：请参考com.acooly.module.openapi.client.provider.wft.OpenAPIClientWftConfigration.getApiSDKServlet
   * 微信公众号支付：/gateway/notify/wftNotify/payWeixinJspay
   * 支付宝主扫支付：/gateway/notify/wftNotify/payAlipayNative
   * 微信主扫支付：/gateway/notify/wftNotify/payWeixinNative
   * 微信APP支付：/gateway/notify/wftNotify/payWeixinRawApp
   * 接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中serviceKey方法需要绑定的key值在枚举WftServiceEnum.getKey()可以获取到
   异步通知接收示例：
```java
   @Service
   @Slf4j
   public class WftPayAlipayNativeNotifyService implements NotifyHandler {
       @Override
       public void handleNotify(ApiMessage notify) {
           log.info("威富通支付宝主扫异步通知报文:{}", JSON.toJSONString(notify));
           WftPayAlipayNativeNoitfy alipayNativeNoitfy = (WftPayAlipayNativeNoitfy) notify;
       }
   
       @Override
       public String serviceKey() {
           return WftServiceEnum.PAY_ALIPAY_NATIVE.getKey();
       }
   }
```

###接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum枚举里面：
* code:表示威富通接口编码
* key:表示组建为每个接口定义的唯一标识
* message:表示接口名称

<font color='#DC143C'>注意：</font>调用接口的时候只需要传入响应的业务参数，公共参数如商户号，接口的版本号非特殊情况不需要手动传入，charset默认为UTF-8
     配置文件方式不用传入请求地址

###sdk提供了单独签名、验签、异步实体转化方法

###本组建支持密钥和商户号手动传入方式
* 主要解决密钥托管。由于异步通知需要验签，所以如果使用密钥托管方式异步通知需要动态根据商户号获取对应的密钥，解决方案如下代码
```java
    @Component
    public class WftLoadKeyStoreService extends AbstractKeyLoadManager<String> implements KeySimpleLoader {
    
        @Autowired
        private BankKeyManageService bankKeyManageService;
    
        @Override
        public String doLoad(String principal) {
            BankKeyManage bankKeyManage = bankKeyManageService.getBankKeyByPartnerId(principal);
            if (bankKeyManage == null) {
                throw new BusinessException("密钥加载失败");
            } else {
                return bankKeyManage.getPartnerKey();
            }
        }
    
        @Override
        public String getProvider() {
            return WftConstants.PROVIDER_NAME;
        }
    }
```

###非密钥托管方式配置文件示例(密钥托管只是不需要配置商户号和密钥)：
* 组建是否启用标识
acooly.openapi.client.fuyou.enable=true
* 服务请求地址
acooly.openapi.client.wft.gatewayUrl=https://pay.swiftpass.cn/pay/gateway
* 请求连接超时时间
acooly.openapi.client.wft.connTimeout=10000
* 响应超时时间
acooly.openapi.client.wft.readTimeout=30000
* 商户号
acooly.openapi.client.wft.partnerId=7551000001
* 商户密钥
acooly.openapi.client.wft.privateKey=9d101c97133837e13dde2d32a5054abb
* 调用系统访问根路径
acooly.openapi.client.wft.domain=http://218.70.106.250:8881
* 对账文件存放路径
acooly.openapi.client.wft.filePath=D:/var
