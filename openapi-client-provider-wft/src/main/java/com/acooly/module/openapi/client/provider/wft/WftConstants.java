/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.wft;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class WftConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "wftProvider";
    public static final String ORDER_NO_NAME = "out_trade_no";

    public static final String SIGN = "sign";
    public static final String SIGN_TYPE = "MD5";
    public static final String SIGNER_TYPE = "wftMd5";

}
