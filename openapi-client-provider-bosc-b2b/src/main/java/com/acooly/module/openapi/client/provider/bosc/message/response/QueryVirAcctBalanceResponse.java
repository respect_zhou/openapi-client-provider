package com.acooly.module.openapi.client.provider.bosc.message.response;

import lombok.Getter;
import lombok.Setter;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;

@Getter
@Setter
public class QueryVirAcctBalanceResponse extends BoscResponseDomain {

	/**
	 * 账户余额
	 */
	private Money amount;

}
