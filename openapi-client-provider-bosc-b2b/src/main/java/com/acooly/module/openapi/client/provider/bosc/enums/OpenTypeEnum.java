package com.acooly.module.openapi.client.provider.bosc.enums;

public enum OpenTypeEnum {

	COMPANY("company", "企业"),

	PERSON("person", "个人");

	private String code;
	private String message;

	private OpenTypeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
