package com.acooly.module.openapi.client.provider.bosc.domain;

import com.acooly.module.openapi.client.common.enums.ResultCodeEnum;
import com.acooly.module.openapi.client.common.enums.TransStatusEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoscResponseDomain extends BoscBaseDomain {

	private String resultCode;

	private String resultDesc;

	private ResultCodeEnum resultCodeEnum;

	private TransStatusEnum transStatusEnum;

}
