package com.acooly.module.openapi.client.provider.hx.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.hx.domain.HxApiMsgInfo;
import com.acooly.module.openapi.client.provider.hx.domain.HxResponse;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.acooly.module.openapi.client.provider.hx.message.xStream.common.RespWithdrawHead;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.response.RespWithdrawBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Rep")
@HxApiMsgInfo(service = HxServiceEnum.hxWithdraw, type = ApiMessageType.Response)
public class HxWithdrawResponse extends HxResponse {

    @XStreamAlias("Head")
    private RespWithdrawHead respWithdrawHead;

    @XStreamAlias("Body")
    private RespWithdrawBody respWithdrawBody;

}
