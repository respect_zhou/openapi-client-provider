/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.INTERCEPT_WITHDRAW, type = ApiMessageType.Request)
public class InterceptWithdrawRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 提现请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String withdrawRequestNo;
	
	public InterceptWithdrawRequest () {
		setService (BoscServiceNameEnum.INTERCEPT_WITHDRAW.code ());
	}
	
	public InterceptWithdrawRequest (String requestNo, String withdrawRequestNo) {
		this();
		this.requestNo = requestNo;
		this.withdrawRequestNo = withdrawRequestNo;
	}
	
	public String getWithdrawRequestNo () {
		return withdrawRequestNo;
	}
	
	public void setWithdrawRequestNo (String withdrawRequestNo) {
		this.withdrawRequestNo = withdrawRequestNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
}