/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
public class FreezeDetailInfo {
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max=50)
	private String platformUserNo;
	/**
	 * 冻结流水号
	 */
	@NotEmpty
	@Size(max=50)
	private String requestNo;
	/**
	 * 冻结金额
	 */
	@MoneyConstraint
	private Money amount;
	/**
	 * 累计解冻金额
	 */
	@MoneyConstraint
	private Money unfreezeAmount;
	/**
	 * FREEZED 表示尚有冻结，UNFREEZED 始化，ERROR 表示异常
	 */
	@NotEmpty
	@Size(max=50)
	private String status;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 交易完成时间
	 */
	private Date transactionTime;
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getUnfreezeAmount () {
		return unfreezeAmount;
	}
	
	public void setUnfreezeAmount (Money unfreezeAmount) {
		this.unfreezeAmount = unfreezeAmount;
	}
	
	public String getStatus () {
		return status;
	}
	
	public void setStatus (String status) {
		this.status = status;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
}
