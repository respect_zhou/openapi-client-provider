/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.jyt.domain;

import com.acooly.module.openapi.client.provider.jyt.message.dto.JytHeaderResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhangpu
 */
@Getter
@Setter
public class JytResponse extends JytApiMessage {

	/**
	 * 响应报文头信息
	 */
	@XStreamAlias("head")
	private JytHeaderResponse headerResponse;
}
