package com.acooly.module.openapi.client.provider.allinpay.message;

import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayApiMsgInfo;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayRequest;
import com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayServiceEnum;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpaySinglePaymentRequestBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 17:15
 * @Description:
 */
@Getter
@Setter
@XStreamAlias("AIPG")
@AllinpayApiMsgInfo(service = AllinpayServiceEnum.SINGLE_PAYMENT)
public class AllinpaySinglePaymentRequest extends AllinpayRequest {

    /**
     * 业务参数对象
     */
    @XStreamAlias("TRANS")
    private AllinpaySinglePaymentRequestBody requestBody;
}
