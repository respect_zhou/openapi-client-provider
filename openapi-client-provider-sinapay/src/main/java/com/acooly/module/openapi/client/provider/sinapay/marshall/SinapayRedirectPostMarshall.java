/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.sinapay.SinapayConstants;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 跳转请求报文组装
 *
 * @author zhike 2017-09-24 16:11
 */
@Slf4j
@Component
public class SinapayRedirectPostMarshall extends SinapayAbstractMarshall implements ApiMarshal<PostRedirect, SinapayRequest> {

    @Override
    public PostRedirect marshal(SinapayRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        postRedirect.setRedirectUrl(SinapayConstants.getCanonicalUrl(getProperties().getMemberGatewayUrl(),
                SinapayConstants.getServiceName(source)));
        postRedirect.addData(SinapayConstants.REQUEST_PARAM_NAME, doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }


}