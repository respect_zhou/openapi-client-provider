/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月7日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;

/**
 * @author zhike
 */
@SinapayApiMsg(service = SinapayServiceNameEnum.AUDIT_MEMBER_INFOS, type = ApiMessageType.Response)
public class AuditMemberInfosResponse extends SinapayResponse {

}
