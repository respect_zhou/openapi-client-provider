/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月12日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.enums;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public enum SinaWithdrawStatus {

	INIT("INIT", "初始化"),  			//新浪支付安全模式订单状态

	SUCCESS("SUCCESS", "成功"),			//系统会异步通知

	FAILED("FAILED", "失败"),			//系统会异步通知

	PROCESSING("PROCESSING", "交易中"),	//系统不会异步通知

	RETURNT_TICKET("RETURNT_TICKET", "退票");	//系统会异步通知

	public static boolean isSuccess(String code) {
		return find(code) == SUCCESS;
	}

	private final String code;
	private final String message;

	private SinaWithdrawStatus(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String code() {
		return code;
	}

	public String message() {
		return message;
	}

	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (SinaWithdrawStatus type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}

	/**
	 * 通过枚举值码查找枚举值。
	 * 
	 * @param code
	 *            查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException
	 *             如果 code 没有对应的 Status 。
	 */
	public static SinaWithdrawStatus find(String code) {
		for (SinaWithdrawStatus status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("SinaWithdrawStatus not legal:" + code);
	}

	/**
	 * 获取全部枚举值。
	 * 
	 * @return 全部枚举值。
	 */
	public static List<SinaWithdrawStatus> getAll() {
		List<SinaWithdrawStatus> list = new ArrayList<SinaWithdrawStatus>();
		for (SinaWithdrawStatus status : values()) {
			list.add(status);
		}
		return list;
	}

	/**
	 * 获取全部枚举值码。
	 * 
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (SinaWithdrawStatus status : values()) {
			list.add(status.code());
		}
		return list;
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.code, this.message);
	}
}
