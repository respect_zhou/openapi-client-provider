/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.BINDING_BANK_CARD_ADVANCE, type = ApiMessageType.Response)
public class BindingBankCardAdvanceResponse extends SinapayResponse {

	/** 绑卡ID */
	@Size(max = 32)
	@NotEmpty
	@ApiItem(value = "card_id")
	private String cardId;

	/**
	 * 是否采用卡认证的方式
	 * 
	 * 如果之前请求中此卡采用的是卡认证的方式（verify_mode不为空），则返回Y，注意此参数和卡是否通过银行认证无关
	 */
	@Size(max = 1)
	@NotEmpty
	@ApiItem(value = "is_verified")
	private String isVerified;
}
