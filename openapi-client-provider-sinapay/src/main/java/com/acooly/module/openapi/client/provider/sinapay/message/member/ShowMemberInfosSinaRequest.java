package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:29
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.SHOW_MEMBER_INFOS_SINA, type = ApiMessageType.Request)
public class ShowMemberInfosSinaRequest extends SinapayRequest {

    /**
     *用户标识信息
     * 商户系统用户ID(字母或数字)
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     *用户标识类型
     * ID的类型，目前只包括UID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = "UID";

    /**
     *响应方式
     * 1：URL返回
     */
    @NotEmpty
    @Size(max = 1)
    @ApiItem(value = "resp_method")
    private String respMethod = "1";

    /**
     *默认展示页面
     * 指定展示页面的默认值。目前共有5个：
     * DEFAULT（默认页面即账户总览）
     * SAFETY_CENTER（安全中心）
     * WITHHOLD（我的授权页面）
     * ORDER(订单查询)
     */
    @Size(max = 50)
    @ApiItem(value = "default_page")
    private String defaultPage;

    /**
     *隐藏页面
     *隐藏页面，页面参考上面的列表，可隐藏多个页面，使用”|”分隔。
     */
    @Size(max = 200)
    @ApiItem(value = "hide_pages")
    private String hidePages;

    /**
     *定制化模板配置
     *页面控制定制化参数模板配置，可与单项共同使用。单项配置会覆盖模板配置。
     * 目前只提供“1001”。不填时默认“1001”
     * 模板参数参见说明。
     */
    @Size(max = 50)
    @ApiItem(value = "templet_custom")
    private String templetCustom;

    /**
     *单项定制化配置
     *定制页面控制参数的单项，
     * 参数格式：页面名.参数名1^参数值1|页面名.参数名2^参数值2|…….
     * 定制项参见说明
     */
    @Size(max = 1500)
    @ApiItem(value = "single_custom")
    private String singleCustom;

}
