/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member.dto;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike
 */
@ApiDto
@Getter
@Setter
public class BonusInfo implements Dtoable {

	/** 昨日收益 */
	@MoneyConstraint
	@ItemOrder(0)
	private Money day;

	/** 最近一月收益 */
	@MoneyConstraint
	@ItemOrder(1)
	private Money month;

	/** 总收益 */
	@MoneyConstraint
	@ItemOrder(2)
	private Money total;

	@Override
	public String toString() {
		return "BonusInfo [day=" + day + ", month=" + month + ", total=" + total + "]";
	}

}
