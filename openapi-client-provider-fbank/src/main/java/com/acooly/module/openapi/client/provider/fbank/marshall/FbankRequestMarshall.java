/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.fbank.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.utils.RSAUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Base64;

/**
 * @author zhike
 */
@Service
@Slf4j
public class FbankRequestMarshall extends FbankMarshallSupport implements ApiMarshal<String, FbankRequest> {

    @Override
    public String marshal(FbankRequest source) {
        beforeMarshall(source);
        String requestData = doMarshall(source);
        log.info("请求报文明文：{}", requestData);
        String encryptRequestDate = Base64.getEncoder().encodeToString(RSAUtil.encryptByPublicKeyByPKCS1Padding(requestData.getBytes(), getProperties().getPublicKey()));
        return encryptRequestDate;
    }

}
