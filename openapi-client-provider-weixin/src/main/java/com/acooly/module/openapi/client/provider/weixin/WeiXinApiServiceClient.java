package com.acooly.module.openapi.client.provider.weixin;

import java.util.Map;

import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.weixin.domain.WeixinNotify;
import com.acooly.module.openapi.client.provider.weixin.domain.WeixinRequest;
import com.acooly.module.openapi.client.provider.weixin.domain.WeixinResponse;
import com.acooly.module.openapi.client.provider.weixin.marshall.WeixinNotifyUnmarshall;

import lombok.extern.slf4j.Slf4j;

/**
 * ApiService执行器
 *
 * @author zhangpu
 */
@Component("weixinApiServiceClient")
@Slf4j
public class WeixinApiServiceClient //implements ApiServiceClient<WeixinRequest, WeixinResponse, WeixinNotify, WeixinNotify>{
        extends AbstractApiServiceClient<WeixinRequest, WeixinResponse, WeixinNotify, WeixinNotify> {


	public static final String PROVIDER_NAME = "weixin";
	
	@Autowired
	private OpenAPIClientWeixinProperties openAPIClientWeixinProperties;
	
	@Autowired
    private WeixinNotifyUnmarshall notifyUnmarshal;
	
	@Override
	public WeixinResponse execute(WeixinRequest request) {
		return null;
	}


	@Override
	public String redirectGet(WeixinRequest request) {
		return null;
	}


	@Override
	public PostRedirect redirectPost(WeixinRequest request) {
		return null;
	}


	@Override
	public WeixinNotify notice(Map<String, String> notifyData, String serviceKey) {
		try {
			WeixinNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
			return notify;
		} catch (ApiClientException e) {
			log.warn("通知验签客户端:{}",e.getMessage());
			throw new ApiClientException("客户端:" + e.getMessage());
		} catch (Exception e) {
			log.error("通知验签内部错误",e);
			throw new ApiClientException("内部错误:" + e.getMessage());
		}
	}


	@Override
	public WeixinNotify result(Map<String, String> notifyData, String serviceKey) {
		return null;
	}

	protected void beforeExecute(WeixinRequest request) {
		MDC.put("service", request.getService());
	}

	@Override
	public String getName() {
		return PROVIDER_NAME;
	}


	@Override
	protected ApiMarshal<String, WeixinRequest> getRequestMarshal() {
		return null;
	}


	@Override
	protected ApiUnmarshal<WeixinResponse, String> getResponseUnmarshal() {
		return null;
	}


	@Override
	protected ApiMarshal<String, WeixinRequest> getRedirectMarshal() {
		return null;
	}


	@Override
	protected ApiUnmarshal<WeixinNotify, Map<String, String>> getNoticeUnmarshal() {
		return notifyUnmarshal;
	}


	@Override
	protected ApiUnmarshal<WeixinNotify, Map<String, String>> getReturnUnmarshal() {
		return null;
	}


	@Override
	protected Transport getTransport() {
		return null;
	}
}
