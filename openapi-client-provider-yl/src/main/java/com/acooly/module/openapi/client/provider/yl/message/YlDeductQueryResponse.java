package com.acooly.module.openapi.client.provider.yl.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlResponse;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_REAL_DEDUCT_QUERY, type = ApiMessageType.Response)
public class YlDeductQueryResponse extends YlResponse {

    /**
     * 交易类型
     */
    String bizType;

    /**
     * 账号 (银行卡或存折号码)
     */
    String payBankCardNo;

    /**
     * 账号名   (客户姓名)
     */
    String payRealName;

}
