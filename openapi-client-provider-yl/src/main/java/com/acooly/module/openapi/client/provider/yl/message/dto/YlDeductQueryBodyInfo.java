package com.acooly.module.openapi.client.provider.yl.message.dto;

import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class YlDeductQueryBodyInfo implements Serializable {

    /**
     * 记录序号 (同一个请求内必须唯一。从0001开始递增)
     */
    @Length(min = 1, max = 25)
    private String bizOrderNo;

    /**
     * 0，批次未完成时，不返回明细
     * 1，批次未完成时，返回所有明细
     */
    private String retType;

    /**
     * 备注
     */
    private String memo;

}
